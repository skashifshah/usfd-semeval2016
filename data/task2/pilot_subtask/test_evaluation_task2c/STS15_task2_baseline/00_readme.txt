*********************
STS15 TASK2 BASELINE
*********************

Simple python implementation of an aligner system.




**************
EXAMPLE
**************

Execute this command to align sentences from STSint.input.headlines.sent1.chunk.txt.utf8 and STSint.input.headlines.sent2.chunk.txt.utf8 when gold standard chunks are provided.

$ ./02_run_baseline_aligner.sh STSint.input.headlines.sent1.chunk.txt.utf8 STSint.input.headlines.sent2.chunk.txt.utf8 True

[ ... ]

<sentence id="378" status="">
// Egypt ministry again urges end to pro-Morsi protests
// Egypt : Child killed in Cairo clashes after pro-Morsi protest
<source>
1 Egypt : 
2 ministry : 
3 again : 
4 urges : 
5 end : 
6 to : 
7 pro-Morsi : 
8 protests : 
</source>
<translation>
1 Egypt : 
2 : : 
3 Child : 
4 killed : 
5 in : 
6 Cairo : 
7 clashes : 
8 after : 
9 pro-Morsi : 
10 protest : 
</translation>
<alignment>
1 2 <==> 1 // EQUI // 5 // Egypt ministry <==> Egypt
6 7 8 <==> 8 9 10 // EQUI // 5 // to pro-Morsi protests <==> after pro-Morsi protest
3 <==> 0 // NOALI // NIL // again <==> -not aligned-
4 <==> 0 // NOALI // NIL // urges <==> -not aligned-
5 <==> 0 // NOALI // NIL // end <==> -not aligned-
0 <==> 3 // NOALI // NIL // -not aligned- <==> Child
0 <==> 4 // NOALI // NIL // -not aligned- <==> killed
0 <==> 5 6 7 // NOALI // NIL // -not aligned- <==> in Cairo clashes
0 <==> 2 // NOALI // NIL // -not aligned- <==> :
</alignment>



*************
REQUIREMENTS
*************

maven 3 (set path of maven executable in 01_execute_first_time.sh script)
git
perl 5
python 2.7.5+

(tested under bash interpreter)




*****************************
EXECUTING FOR THE FIRST TIME
*****************************

$ ./01_execute_first_time.sh

This script will copy ixa-pipes official binary files (ixa-pipes-chunk is manually build, see 01_execute_first_time.sh).

ixa-pipe-tok: Tokenizer and Segmenter for several languages.
ixa-pipe-pos: POS tagger for Spanish and English.
ixa-pipe-nerc: Named Entity Recognition tagger for Spanish and English.
ixa-pipe-parse: Probabilistic constituent parser for Spanish and English.

IXA pipes is a modular set of Natural Language Processing tools (or pipes) which provide easy access to NLP technology for English and Spanish.
It offers robust and efficient linguistic annotation to both researchers and non-NLP experts with the aim of lowering the barriers of using NLP technology either for research purposes or for small industrial developers and SMEs.
The ixa-pipes tools can be used or exploit its modularity to pick and change different components.
The tools are developed by the IXA NLP Group of the University of the Basque Country (http://ixa2.si.ehu.es/ixa-pipes/).

If you use the ixa pipes tools or the models, please cite this paper:
Rodrigo Agerri, Josu Bermudez and German Rigau (2014): "IXA pipeline: Efficient and Ready to Use Multilingual NLP tools", in: Proceedings of the 9th Language Resources and Evaluation Conference (LREC2014), 26-31 May, 2014, Reykjavik, Iceland. PDF paper

Every ixa pipe tool can be up an running after two simple steps.
The tools require Java 1.7+ to run and are designed to come with all batteries included, which means that it is not required to do any system configuration or install any third-party dependencies.
The modules will run on any platform as long as a JVM 1.7+ is available.


*****************************
EXECUTING THE ALIGNER SYSTEM
*****************************

First argument: File containing first sentences in input sentence pairs
Second argument: File containing second sentences in input sentence pairs
Third argument: Whether input sentences are chunked or not (True / False)
  If False, we use ixa-pipes to chunk the input files.

Notice that all arguments are required


$ ./02_run_baseline_aligner.sh STSint.input.headlines.sent1.chunk.txt.utf8 STSint.input.headlines.sent2.chunk.txt.utf8 True > myOutput.wa

$ ./wellformed.pl myOutput.wa 
Well-formedness of myOutput.wa: correct

perl evalF1.pl STSint.gs.headlines.wa myOutput.wa 
 F1 Ali     0.8246
 F1 Type    0.5326
 F1 Score   0.7277
 F1 Typ+Sco 0.6477


********
CONTACT
********

inigo.lopez@ehu.es




****************
ABOUT COPYRIGHT
****************

This software uses munkres 1.0.6 (https://pypi.python.org/pypi/munkres/), which is licensed under a BSD license, adapted from <http://opensource.org/licenses/bsd-license.php (Copyright (c) 2008 Brian M. Clapper All rights reserved).
Please visit

    http://www.public.iastate.edu/~ddoty/HungarianAlgorithm.html
    Harold W. Kuhn. The Hungarian Method for the assignment problem. Naval Research Logistics Quarterly, 2:83-97, 1955.
    Harold W. Kuhn. Variants of the Hungarian method for assignment problems. Naval Research Logistics Quarterly, 3: 253-258, 1956.
    Munkres, J. Algorithms for the Assignment and Transportation Problems. Journal of the Society of Industrial and Applied Mathematics, 5(1):32-38, March, 1957.
    http://en.wikipedia.org/wiki/Hungarian_algorithm

for more information

