#!/bin/bash

MAVEN='/media/guest/data/Data/IXA/Maven/maven3/apache-maven-3.0.5/bin/mvn'

mkdir ixa-pipes-zerbitzarian
cd ixa-pipes-zerbitzarian
wget http://ixa2.si.ehu.es/ixa-pipes/models/ixa-pipes-1.0.0-bin.tar.gz
tar xvzf ixa-pipes-1.0.0-bin.tar.gz
mv ixa-pipes-1.0.0-bin/*.jar .

####
# THE IXA-PIPE-CHUNK MODULE MUST BE INSTALLED MANUALLY
# IT WILL SOON BE ABLE AS BINARY IN THE PREVIOUS PACKAGE

git clone https://github.com/ixa-ehu/ixa-pipe-chunk.git
cd ixa-pipe-chunk/src/main/resources
wget http://ixa2.si.ehu.es/ixa-pipes/models/chunk-resources.tgz
tar xvzf chunk-resources.tgz
cd -
cd ixa-pipe-chunk
$MAVEN clean package
cp target/ixa-pipe-chunk-1.0.jar ../
