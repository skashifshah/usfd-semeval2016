﻿                               SemEval 2016 Task 1

                  Crosslingual Semantic Textual Similarity (STS)
                                 Pilot Subtask


This package contains the trial data for the crosslingual semantic textual
similarity (STS) pilot subtask of SemEval 2016 Task 1.

The following files are provided:

 00-README.txt                      This file.
 correlation.pl                     Evaluation script for a single dataset.
 correct-output.pl                  System output validation script.
 STS.input.crosslingual-trial.txt   Tab separated sample input file with
                                    crosslingual sentence pairs.
 STS.gs.crosslingual-trial.txt      Gold standard annotations.
 STS.output.crosslingual-trial.txt  Sample system output.

Introduction
------------

STS 2016 consists of two tracks, namely a core monolingual English STS subtask
and a pilot subtask on crosslingual STS featuring paired sentences written in
English and Spanish.

This README describes the trial data for the crosslingual pilot subtask.

The crosslingual STS task is similar to the core STS task in that given two
statements, s1 and s2, systems should compute the similarity of the underlying
semantics of the pair and return a continuous valued similarity score between 0
and 5. The crosslingual STS task differs from the core task in that, rather than
just containing sentences in a single language (e.g., English), the crosslingual
data contains paired sentences in two different languages (namely English and
Spanish).

Trial Data
------------

For the 2016 pilot subtask, each pair contains an English sentence and a Spanish
sentence. The trial data was drawn from sentence pairs used in prior English STS
evaluations (STS 2012, 2013, 2014, and 2015). The English STS sentence pairs
were sampled at random and one of the sentences in each pair was translated by
a native Spanish speaker. The semantic similarity score for each crosslingual
pair was reused from the English STS annotations. A manual quality control pass
was performed to filter lower quality crosslingual pairs.

The data is approximately balanced across STS scores. Rounding pair scores to
the nearest integer, the pairs contained within the trial data have the
following distribution across STS score levels:

STS Score       Pairs
---------       -----
0               16
1               15
2               12
3               20
4               20
5               20

The test data for the crosslingual pilot subtask will also be approximately
balanced across STS scores. However, the exact distribution of the scores may
differ from the one obtained on the trial data.

Possible Approaches
-------------------

Given that this pilot subtask is the first time a crosslingual STS task has been
attempted, we encourage participants to explore a wide range of possible models
and solutions. Some potentially promising approaches include:
solutions

   -  Adapting Monolingual Align+Featurize STS Systems to the Crosslingual Task

      One of the most successful techniques for monolingual STS is to first
      align the words in a sentence pair, extract features, and then provide
      those features to a machine learning model that predicts the final STS
      score. Crosslingual STS could be approached by adapting such a pipeline
      to the crosslingual setting.

      Open-source crosslingual alignment tools, such as MGIZA++ (Gao and Vogel
      2008), are widely used by the machine translation community, although
      better alignments for STS may be possible with a purpose built aligner.
      Multilingual lexical databases such as the Multilingual Central Repository
      (MCR) (González-Agirre et al. 2013) should allow for the adaptation of
      WordNet based lexical features to the crosslingual setting. For
      distributional features, open source tools that project monolingual
      vector based semantic representations into a sharded crosslingual space
      may prove useful (Faruqui and Dyer 2014).


   -  Deep Learning with Crosslingual Embeddings

      There has been recently very interesting work on embedding models for
      crosslingual lexical and text sequence semantics (i.a., Pham et al. 2015,
      Zhao et al. 2015, Chandar A P et al. 2014, Faruqui and Dyer 2014,
      Zhou et al. 2013). Providing such embeddings to a deep learning model
      could be used to directly solve the crosslingual STS task without the
      feature engineering work that would characterize a typical
      align+featurize STS system.


   -  Monolingual STS System+Machine Translation

      This approach could be taken by extending existing open source monolingual
      STS systems such as DKPro (Bär et al. 2012) or the Takelab STS system
      (Šarić et al 2012) and combining it with a translation system built using
      open source tools such as Moses (Koehn et al. 2007) or Phrasal (Green et
      al. 2014).

Input format
------------

The input file consists of paired crosslingual statements with one pair per
line. The statements in a pair are separated by a tab character:

<1st statement (does not contain tabs)>\t<2nd statement (does not contain tabs)>

File: STS.input.crosslingual-trial.txt

Gold Standard
-------------

The gold standard file contains a score between 0 and 5 for each pair of
statements, with the following interpretation:

(5) The two sentences are completely equivalent, as they mean the same
    thing.

       El pájaro se está bañando en el lavabo.
       Birdie is washing itself in the water basin.

(4) The two sentences are mostly equivalent, but some unimportant
    details differ.

       En mayo de 2010, las tropas intentaron invadir Kabul.
       The US army invaded Kabul on May 7th last year, 2010.

(3) The two sentences are roughly equivalent, but some important
    information differs or is missing.

       John dijo que él es considerado como testigo, y no como sospechoso.
       "He is not a suspect anymore." John said.

(2) The two sentences are not equivalent, but share some details.

       Ellos volaron del nido en grupos.
       They flew into the nest together.

(1) The two sentences are not equivalent, but are on the same topic.

       La mujer está tocando el violín.
       The young lady enjoys listening to the guitar.

(0) The two sentences are on different topics.

       Al amanecer, Juan se fue a montar a caballo con un grupo de amigos.
       Sunrise at dawn is a magnificent view to take in if you wake up
       early enough for it.

The gold standard file format consists of one single field per line:

      - a number between 0 and 5

As in prior years, the gold standard in the test data will be annotated using
mechanical turk and derived from 5 separate human annotations per sentence pair.

File: STS.gs.crosslingual-trial.txt

Answer format
--------------

The answer format is identical to the gold standard format. Each line should
have one number corresponding to your system's predicted STS score for a pair:

      - a number between 0 and 5 (the computed similarity score)

The output file must conform to the above specification. Incorrectly formatted
files will be automatically removed from the evaluation. You can verify that
your answer files are in the proper format using the following script:

      $ ./correct-output.pl STS.output.SMT.txt
      Output file is OK!

In addition to reporting problems and an overall final status message on
standard error, the script also returns 0 if and only if the provided file is
correctly formatted.

File: STS.output.crosslingual-trial.txt

Scoring
-------

The official score is based on weighted Pearson correlation.

The following script returns the correlation for individual pairs:

       $ ./correlation.pl \
          STS.gs.crosslingual-trial.txt \
          STS.output.crosslingual-trial.txt
       Pearson: 0.96028

Participation in the task
-------------------------

Participants will be allowed to submit at most three runs using minor variants
of a single system.

However, participants with two or more substantively different models or
approaches will be allowed to submit up to five different systems. If you plan
on submitting more than three systems please check with the task organizers
first, to see if the methods being used are different enough to warrant more
than three submissions.

NOTE: Participant systems should NOT use any explicit or implicit human
annotations of the following datasets to develop or train their systems:

- Corpus of Plagiarised Short Answers (Clough and Stevenson 2011)
- Stack Exchange Q&A Forums (neither Data Dump, Data Explorer or crawled data)
- Europe Media Monitor (EMM) (Best et al. 2005)
- WMT quality estimation shared task (Callison-Burch et al. 2012)

However, we *explicitly* allow purely unsupervised system components to use
data drawn from any of these sources. Purely unsupervised methods must not
make use of any human annotations or data source structure.

Check with the organizers to see if your use of the restricted data sources is
permissible.

Other
-----

Please check http://ixa2.si.ehu.es/sts for more details.

We highly recommend that potential participants join the task mailing list:

     http://groups.google.com/group/STS-semeval

Organizers
----------

Carmen Banea (University of Michigan)
Daniel Cer (Google)
Rada Mihalcea (University of Michigan)
Janyce Wiebe (University of Pittsburgh)

Acknowledgements
----------------

This work is partially funded by the National Science Foundation
(CAREER award \#1361274), and DARPA-BAA-12-47 (DEFT grant \#12475008).

Any opinions, findings, and conclusions or recommendations expressed in this
material are those of the authors and do not necessarily reflect the views of
the National Science Foundation or DARPA.

References
----------

Semantic Textual Similarity Wiki:

       http://ixa2.si.ehu.es/stswiki/index.php/Main_Page

Daniel Bär, Chris Biemann, Iryna Gurevych and Torsten Zesch. UKP: Computing
Semantic Textual Similarity by Combining Multiple Content Similarity Measures
(SemEval 2012).

       https://code.google.com/p/dkpro-similarity-asl/wiki/SemEval2013

Sarath Chandar A P, Stanislas Lauly, Hugo Larochelle, Mitesh Khapra, Balaraman
Ravindran, Vikas C. Raykar and Amrita Saha. An Autoencoder Approach to Learning
Bilingual Word Representations (NIPS 2014).

Manaal Faruqui and Chris Dyer. Improving Vector Space Word Representations Using
Multilingual Correlation (EACL 2014).

       https://github.com/mfaruqui/eacl14-cca

Qin Gao and Stephan Vogel. Parallel Implementations of Word Alignment Tool
(ACL Soft Eng. Workshop 2008).

       https://github.com/moses-smt/mgiza

Aitor González-Agirre and German Rigau. Construcción de una base de
conocimiento léxico multilíngüe de amplia cobertura: Multilingual Central
Repository. Linguamática. Revista para o Processamento Automático das Línguas
Ibéricas Vol. 5(1). 13-28 - ISSN: 1647-0818. 2013.

       http://adimen.si.ehu.es/web/MCR

Spence Green, Daniel Cer and Christopher D. Manning. Phrasal: A Toolkit for
New Directions in Statistical Machine Translation (WMT 2014).

       https://github.com/stanfordnlp/phrasal

Philipp Koehn, Hieu Hoang, Alexandra Birch, Chris Callison-Burch, Marcello
Federico, Nicola Bertoldi, Brooke Cowan, Wade Shen, Christine Moran, Richard
Zens, Chris Dyer, Ondrej Bojar, Alexandra Constantin and Evan Herbst. Moses:
Open Source Toolkit for Statistical Machine Translation (ACL-demo 2007).

       http://www.statmt.org/moses/

Hieu Pham, Minh-Thang Luong and Christopher D. Manning. Learning Distributed
Representations for Multilingual Text Sequences(NAACL 2015).

Frane Šarić, Goran Glavaš, Mladen Karan, Jan Šnajder and Bojana Dalbelo Bašić.
TakeLab: Systems for Measuring Semantic Text Similarity (SemEval 2012).

       http://takelab.fer.hr/sts/

Kai Zhao, Hany Hassan and Michael Auli. Learning Translation Models from
Monolingual Continuous Representations (NAACL 2015).

Will Zou, Richard Socher, Daniel Cer and Christopher D. Manning. Bilingual
Word Embeddings for Phrase-Based Machine Translation (EMNLP-short 2013).
