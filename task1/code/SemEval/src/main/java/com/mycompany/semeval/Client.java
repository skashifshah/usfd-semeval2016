/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.semeval;

import com.shef.baselines.CosineWordBased;
import com.shef.baselines.SimilarityMeasure;
import com.shef.cmdCalls.CMDCaller;
import com.shef.constants.Constants;
import com.shef.representation.Sentence;
import com.shef.util.DataProcessor;
import com.shef.util.Langs;
import com.shef.util.LanguageProcessor;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author ahmet
 */
public class Client {
    
    public static void main(String args[]) throws IOException {
                CMDCaller.callScript((new File("task1")).getAbsolutePath() + "/scripts/callCorrelation.sh");

    }
    
}
