/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shef.cmdCalls;

import com.shef.util.Util;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.Vector;

/**
 *
 * @author ahmetaker
 */
public class RougeMetricSimilarity {

    public static Vector<Double> getROUGEScore(String aReferenceSummary, String aPeerSummary, boolean isToGenerateConfigFile, boolean isToGenerateReferenceSum) throws IOException {

        if (isToGenerateReferenceSum) {
            try {
                Util.doSave("rougeFiles" + "/model" + "1" + ".spl", aReferenceSummary);
            } catch (Exception e) {
                BufferedWriter writer = new BufferedWriter(new FileWriter("rougeFiles/model1.spl"));
                writer.append(aReferenceSummary);
                writer.flush();
                writer.close();
            }

        }


        try {
            Util.doSave("rougeFiles" + "/summary" + (1) + ".spl", aPeerSummary);
        } catch (Exception e) {
            BufferedWriter writer = new BufferedWriter(new FileWriter("rougeFiles/summary" + (1) + ".spl"));
            writer.append(aPeerSummary);
            writer.flush();
            writer.close();
        }





        if (isToGenerateConfigFile) {
            StringBuffer bufferConfig = new StringBuffer();
            bufferConfig.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>").append("\n");
            bufferConfig.append("<ROUGE-EVAL>").append("\n");

            bufferConfig.append("<EVAL ID=\"" + (1) + "\">").append("\n");
            bufferConfig.append("<PEER-ROOT>").append("d:/work/workspace/SENSEI/rougeFiles").append("</PEER-ROOT>").append("\n");
            bufferConfig.append("<MODEL-ROOT>").append("d:/work/workspace/SENSEI/rougeFiles").append("</MODEL-ROOT>").append("\n");
            bufferConfig.append("<INPUT-FORMAT TYPE=\"SPL\" />").append("\n");
            bufferConfig.append("<PEERS>").append("\n");
            bufferConfig.append("<P ID=\"" + (1) + "\">summary" + (1) + ".spl</P>").append("\n");
            bufferConfig.append("</PEERS>").append("\n");
            bufferConfig.append("<MODELS>").append("\n");

            bufferConfig.append("<M ID=").append("\"").append(1).append("\">").append("model" + "1" + ".spl").append("</M>").append("\n");

            bufferConfig.append("</MODELS>").append("\n");
            bufferConfig.append("</EVAL>").append("\n");

            bufferConfig.append("</ROUGE-EVAL>").append("\n");

            try {
                Util.doSave("rougeFiles" + "/configFileRouge.xml", bufferConfig.toString());
            } catch (Exception e) {
                BufferedWriter writer = new BufferedWriter(new FileWriter("rougeFiles" + "/configFileRouge.xml"));
                writer.append(bufferConfig.toString());
                writer.flush();
                writer.close();
            }
        }
        StreamReader streamReader = CMDCaller.callRouge("d:/work/workspace/SENSEI/rougeFiles" + "/command.bat");
        Vector<Double> list = new Vector<Double>();
        list.add(streamReader.getRouge1Value());
        list.add(streamReader.getRouge2Value());
        list.add(streamReader.getROUGESU4());
        
        return list;
    }
}
