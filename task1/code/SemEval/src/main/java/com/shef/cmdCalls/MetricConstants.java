/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shef.cmdCalls;

/**
 *
 * @author ahmetaker
 */
public class MetricConstants {
    public static String DICE = "dice";
    public static String SUBSTRING = "substring";
    public static String SUBSEQUENCE = "subsequence";
    public static String LEVENSTEIN = "levenstein";
    public static String SUBSTRINGEXACTMATCH = "substringExactMatch";
    
}
