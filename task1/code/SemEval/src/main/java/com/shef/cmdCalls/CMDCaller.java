package com.shef.cmdCalls;

import com.sun.corba.se.impl.javax.rmi.CORBA.Util;

public class CMDCaller {

    public static StreamReader callRouge(String aBatfile) {
        try {
            String[] cmd = new String[1];
            cmd[0] = aBatfile;
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec(cmd);
            // any output?
            StreamReader outputReader = new StreamReader(proc.getInputStream(), "OUTPUT");
            // kick them off
            //errorReader.start();

            outputReader.startMe();

            // any error???
            //int exitVal = proc.waitFor();
            return outputReader;
        } catch (Throwable t) {
            System.out.println(t.getMessage());
        }
        return null;
    }

    public static StreamReaderSeveralEvals callRougeListEval(String aBatfile) {
        try {
            String[] cmd = new String[1];
            cmd[0] = aBatfile;
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec(cmd);
            // any output?
            StreamReaderSeveralEvals outputReader = new StreamReaderSeveralEvals(proc.getInputStream(), "OUTPUT");
            // kick them off
            //errorReader.start();

            outputReader.startMe();

            // any error???
            //int exitVal = proc.waitFor();
            return outputReader;
        } catch (Throwable t) {
            System.out.println(t.getMessage());
        }
        return null;
    }

    public static void callScalaChm(String aBatfile) {
        try {
            String[] cmd = new String[1];
            cmd[0] = aBatfile;
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec(cmd);

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public static void callMert(String aBatfile) {
        try {
            String[] cmd = new String[1];
            cmd[0] = aBatfile;
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec(cmd);
            StreamReader outputReader = new StreamReader(proc.getErrorStream(), "OUTPUT");
            // kick them off
            //errorReader.start();
            outputReader.startMe2();
            //proc.destroy();;
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public static void callScript(String aBatfile) {
        try {
//            String[] cmd = new String[1];
//            cmd[0] = aBatfile;
//            Process rt = Runtime.getRuntime().exec(aBatfile);
//            rt.waitFor();

            ProcessBuilder pb = new ProcessBuilder(aBatfile);
            Process p = pb.start(); // Start the process.
            p.waitFor();

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public static void callGenPoint(String aBatfile) {
        try {
            String[] cmd = new String[1];
            cmd[0] = aBatfile;
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec(cmd);
            StreamReader outputReader = new StreamReader(proc.getInputStream(), "OUTPUT");
            // kick them off
            //errorReader.start();
            outputReader.startMe2();
            //proc.destroy();;
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
}
