package com.shef.cmdCalls;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class StreamReader {

    private InputStream itsStream;
    private String aType;
    private double itsRougeLValue = 0;
    private double itsRouge1Value = 0;
    private double itsRouge2Value = 0;
    private double itsRouge3Value = 0;
    private double itsRouge4Value = 0;
    private double itsRougeWValue = 0;
    private double itsROUGESU = 0;

    public StreamReader() {
    }

    StreamReader(InputStream aStream, String aType) {
        this.itsStream = aStream;
        this.aType = aType;
    }

    /**
     * @return the rouge1Value
     */
    public final double getRouge1Value() {
        return itsRouge1Value;
    }

    /**
     * @param aRouge1Value the rouge1Value to set
     */
    public final void setRouge1Value(double aRouge1Value) {
        itsRouge1Value = aRouge1Value;
    }

    /**
     * @return the rouge2Value
     */
    public final double getRouge2Value() {
        return itsRouge2Value;
    }

    /**
     * @return the rOUGESU
     */
    public final double getROUGESU4() {
        return itsROUGESU;
    }

    /**
     * @param aRougesu the rOUGESU to set
     */
    public final void setROUGESU(double aRougesu) {
        itsROUGESU = aRougesu;
    }

    /**
     * @param aRouge2Value the rouge2Value to set
     */
    public final void setRouge2Value(double aRouge2Value) {
        itsRouge2Value = aRouge2Value;
    }

    /**
     * @return the rouge3Value
     */
    public final double getRouge3Value() {
        return itsRouge3Value;
    }

    /**
     * @param aRouge3Value the rouge3Value to set
     */
    public final void setRouge3Value(double aRouge3Value) {
        itsRouge3Value = aRouge3Value;
    }

    /**
     * @return the rouge4Value
     */
    public final double getRouge4Value() {
        return itsRouge4Value;
    }

    /**
     * @param aRouge4Value the rouge4Value to set
     */
    public final void setRouge4Value(double aRouge4Value) {
        itsRouge4Value = aRouge4Value;
    }

    /**
     * @return the rougeWValue
     */
    public final double getRougeWValue() {
        return itsRougeWValue;
    }

    /**
     * @param aRougeWValue the rougeWValue to set
     */
    public final void setRougeWValue(double aRougeWValue) {
        itsRougeWValue = aRougeWValue;
    }

    /**
     * @param aRougeLValue the rougeLValue to set
     */
    public final void setRougeLValue(double aRougeLValue) {
        itsRougeLValue = aRougeLValue;
    }

    /**
     * @return the rougeValue
     */
    public final double getRougeLValue() {
        return itsRougeLValue;
    }

    public void startMe2() {
        try {
            InputStreamReader isr = new InputStreamReader(itsStream);
            BufferedReader br = new BufferedReader(isr);
            String line = null;

            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
            br.close();
            isr.close();
            itsStream.close();

        } catch (Exception e) {
        }

    }

    public void startMe() {
        try {
            InputStreamReader isr = new InputStreamReader(itsStream);
            BufferedReader br = new BufferedReader(isr);
            String line = null;

            while ((line = br.readLine()) != null) {
                if (line.startsWith("1 ROUGE-L Average_R:")) {
                    line = line.replaceAll("1 ROUGE-L Average_R:", "");
                    line = line.replaceAll("\\(.*", "").trim();
                    itsRougeLValue = Double.parseDouble(line.trim());
                } else if (line.startsWith("1 ROUGE-1 Average_R")) {
                    line = line.replaceAll("1 ROUGE-1 Average_R:", "");
                    line = line.replaceAll("\\(.*", "").trim();
                    itsRouge1Value = Double.parseDouble(line.trim());

                } else if (line.startsWith("1 ROUGE-2 Average_R")) {
                    line = line.replaceAll("1 ROUGE-2 Average_R:", "");
                    line = line.replaceAll("\\(.*", "").trim();
                    itsRouge2Value = Double.parseDouble(line.trim());

                } else if (line.startsWith("1 ROUGE-3 Average_R")) {
                    line = line.replaceAll("1 ROUGE-3 Average_R:", "");
                    line = line.replaceAll("\\(.*", "").trim();
                    itsRouge3Value = Double.parseDouble(line.trim());

                } else if (line.startsWith("1 ROUGE-4 Average_R")) {
                    line = line.replaceAll("1 ROUGE-4 Average_R:", "");
                    line = line.replaceAll("\\(.*", "").trim();
                    itsRouge4Value = Double.parseDouble(line.trim());

                } else if (line.startsWith("1 ROUGE-W-1.2 Average_R")) {
                    line = line.replaceAll("1 ROUGE-W-1.2 Average_R:", "");
                    line = line.replaceAll("\\(.*", "").trim();
                    itsRougeWValue = Double.parseDouble(line.trim());

                } else if (line.startsWith("1 ROUGE-SU4 Average_R")) {
                    line = line.replaceAll("1 ROUGE-SU4 Average_R:", "");
                    line = line.replaceAll("\\(.*", "").trim();
                    itsROUGESU = Double.parseDouble(line.trim());
                }
            }

            br.close();
            isr.close();
            itsStream.close();

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
