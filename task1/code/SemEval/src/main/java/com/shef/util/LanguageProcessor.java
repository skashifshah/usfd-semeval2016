/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shef.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import opennlp.tools.cmdline.postag.POSModelLoader;
import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
//import opennlp.tools.lang.spanish.SentenceDetector;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.InvalidFormatException;
import opennlp.tools.util.Span;
import opennlp.uima.doccat.LanguageDetector;

/**
 *
 * @author ahmetaker
 */
public class LanguageProcessor {

    private static POSModel itsPOSModel = null;
    private static SentenceModel itsSentenceModel = null;
    private static TokenizerModel itsTokenizerModel = null;
    private static TokenNameFinderModel itsNERModelEN_PERSON = null;
    private static TokenNameFinderModel itsNERModelEN_LOC = null;
    private static TokenNameFinderModel itsNERModelEN_ORG = null;
    private static TokenNameFinderModel itsNERModel = null;
    private static NameFinderME itsNameFinder = null;
    private static NameFinderME itsNameFinderEN_PERSON = null;
    private static NameFinderME itsNameFinderEN_LOC = null;
    private static NameFinderME itsNameFinderEN_ORG = null;
    private static TokenizerME itsTokenizer = null;
    private static SentenceDetectorME itsSentenceDetector = null;
    private static POSTaggerME itsPOSTagger = null;

    private static String itsLang = null;
    private static String itsResourceFolder = null;

    public static void setUp(String aLang, String aResourceFolder) {
        itsLang = aLang;
        itsResourceFolder = aResourceFolder;
    }

    public static String[] tokenize(String aSentence, String aLang, String aResourceFolder) throws InvalidFormatException, IOException {
        if (itsTokenizerModel == null) {
            InputStream is = new FileInputStream(aResourceFolder + "/tokenizerModels/" + aLang + "-token.bin");
            itsTokenizerModel = new TokenizerModel(is);
            itsTokenizer = new TokenizerME(itsTokenizerModel);
            is.close();
        }
        String tokens[] = itsTokenizer.tokenize(aSentence);

        //now apply also some rules!
        ArrayList<String> array = new ArrayList<String>();
        for (int i = 0; i < tokens.length; i++) {
            String token = tokens[i].trim();
            if ("".equals(token)) {
                continue;
            }
            char chraters[] = token.toCharArray();
            Vector<String> take = new Vector<String>();
            StringBuffer buffer = new StringBuffer();
            for (int j = 0; j < chraters.length; j++) {
                String c = chraters[j] + "";
                if (Heuristics.isPunctuation(c)) {
                    String str = buffer.toString().trim();
                    if (!str.equals("")) {
                        take.add(buffer.toString());
                    }
                    buffer = new StringBuffer();
                    take.add(c);
                } else {
                    buffer.append(c);
                }
            }
            if (!buffer.toString().equals("")) {
                take.add(buffer.toString());
            }
            for (int j = 0; j < take.size(); j++) {
                String string = take.get(j);
                array.add(string);
            }
        }

        String a[] = new String[array.size()];
        return array.toArray(a);

    }

    public static String[] getNE(String aSentence[]) throws FileNotFoundException, IOException {
        String nes[] = null;
        if (itsLang.equalsIgnoreCase(Langs.EN)) {
            if (itsNERModelEN_PERSON == null) {
                InputStream is = new FileInputStream(itsResourceFolder + "/ners/" + itsLang + "-ner-person.bin");
                itsNERModelEN_PERSON = new TokenNameFinderModel(is);
                is.close();
                itsNameFinderEN_PERSON = new NameFinderME(itsNERModelEN_PERSON);

                is = new FileInputStream(itsResourceFolder + "/ners/" + itsLang + "-ner-location.bin");
                itsNERModelEN_LOC = new TokenNameFinderModel(is);
                is.close();
                itsNameFinderEN_LOC = new NameFinderME(itsNERModelEN_LOC);

                is = new FileInputStream(itsResourceFolder + "/ners/" + itsLang + "-ner-organization.bin");
                itsNERModelEN_ORG = new TokenNameFinderModel(is);
                is.close();
                itsNameFinderEN_ORG = new NameFinderME(itsNERModelEN_ORG);
            }
            Span nameSpansPERSON[] = itsNameFinderEN_PERSON.find(aSentence);
            Span nameSpansLOC[] = itsNameFinderEN_LOC.find(aSentence);
            Span nameSpansORG[] = itsNameFinderEN_ORG.find(aSentence);

            int size = nameSpansPERSON.length + nameSpansLOC.length + nameSpansORG.length;
            nes = new String[size];

            int index = 0;
            for (Span s : nameSpansPERSON) {
                int start = s.getStart();
                int end = s.getEnd();
                StringBuffer buffer = new StringBuffer();
                for (int i = start; i < end; i++) {
                    buffer.append(aSentence[i]).append(" ");
                }
                nes[index++] = buffer.toString();
            }

            for (Span s : nameSpansLOC) {
                int start = s.getStart();
                int end = s.getEnd();
                StringBuffer buffer = new StringBuffer();
                for (int i = start; i < end; i++) {
                    buffer.append(aSentence[i]).append(" ");
                }
                nes[index++] = buffer.toString();
            }

            for (Span s : nameSpansORG) {
                int start = s.getStart();
                int end = s.getEnd();
                StringBuffer buffer = new StringBuffer();
                for (int i = start; i < end; i++) {
                    buffer.append(aSentence[i]).append(" ");
                }
                nes[index++] = buffer.toString();
            }

        } else {
            if (itsNERModel == null) {
                InputStream is = new FileInputStream(itsResourceFolder + "/ners/" + itsLang + "-ner.bin");
                TokenNameFinderModel model = new TokenNameFinderModel(is);
                is.close();
                itsNameFinder = new NameFinderME(model);
            }
            Span nameSpans[] = itsNameFinder.find(aSentence);
            nes = new String[nameSpans.length];
            int index = 0;
            for (Span s : nameSpans) {
                int start = s.getStart();
                int end = s.getEnd();
                StringBuffer buffer = new StringBuffer();
                for (int i = start; i < end; i++) {
                    buffer.append(aSentence[i]).append(" ");
                }
                nes[index++] = buffer.toString();
            }

        }

        return nes;
    }

    public static String[] tokenize(String aSentence) throws InvalidFormatException, IOException {
        if (itsTokenizerModel == null) {
            InputStream is = new FileInputStream(itsResourceFolder + "/tokenizerModels/" + itsLang + "-token.bin");
            itsTokenizerModel = new TokenizerModel(is);
            itsTokenizer = new TokenizerME(itsTokenizerModel);
            is.close();
        }
        String tokens[] = itsTokenizer.tokenize(aSentence);

        //now apply also some rules!
        ArrayList<String> array = new ArrayList<String>();
        for (int i = 0; i < tokens.length; i++) {
            String token = tokens[i].trim();
            if ("".equals(token)) {
                continue;
            }
            char chraters[] = token.toCharArray();
            Vector<String> take = new Vector<String>();
            StringBuffer buffer = new StringBuffer();
            for (int j = 0; j < chraters.length; j++) {
                String c = Character.toString(chraters[j]);
                if (Heuristics.isPunctuation(c)) {
                    String str = buffer.toString().trim();
                    if (!str.equals("")) {
                        take.add(buffer.toString());
                    }
                    buffer = new StringBuffer();
                    take.add(c);
                } else {
                    buffer.append(c);
                }
            }
            if (!buffer.toString().equals("")) {
                take.add(buffer.toString());
            }
            for (int j = 0; j < take.size(); j++) {
                String string = take.get(j);
                array.add(string);
            }
        }

        String a[] = new String[array.size()];
        return array.toArray(a);

    }

    public static Map<String, Vector<String>> getNgram(String aTokenList[], String aSentence[], int aNumberOfTokens, boolean isInSmallLetter) throws IOException {
        Map<String, Vector<String>> ngrams = new HashMap<String, Vector<String>>();
        for (int i = 0; i < aSentence.length - (aNumberOfTokens - 1); i++) {
            StringBuffer ngram = new StringBuffer();
            StringBuffer tokenGram = new StringBuffer();
            for (int j = 0, k = i; j < aNumberOfTokens; j++, k++) {
                ngram.append(aSentence[k].trim()).append("\t");
                tokenGram.append(aTokenList[k].trim()).append("\t");
            }
            String ngramString = ngram.toString().trim();
            String tokenString = tokenGram.toString().trim();
            if (isInSmallLetter) {
                tokenString = tokenString.toLowerCase();
            }
            Vector<String> list = ngrams.get(ngramString);
            if (list == null) {
                list = new Vector<String>();
                ngrams.put(ngramString, list);
            }
            list.add(tokenString);
        }
        return ngrams;
    }

    public static String[] sentenceDetect(String aText, String aLang, String aResourceFolder) throws InvalidFormatException, IOException {
        if (itsSentenceModel == null) {
            InputStream is = new FileInputStream(aResourceFolder + "/setenceDetectionModels/" + aLang + "-sent.bin");
            itsSentenceModel = new SentenceModel(is);
            itsSentenceDetector = new SentenceDetectorME(itsSentenceModel);
            is.close();
        }

        String sentences[] = itsSentenceDetector.sentDetect(aText);
        return sentences;
    }

    public static String[] sentenceDetect(String aText) throws InvalidFormatException, IOException {
        if (itsSentenceModel == null) {
            InputStream is = new FileInputStream(itsResourceFolder + "/setenceDetectionModels/" + itsLang + "-sent.bin");
            itsSentenceModel = new SentenceModel(is);
            itsSentenceDetector = new SentenceDetectorME(itsSentenceModel);
            is.close();
        }

        String sentences[] = itsSentenceDetector.sentDetect(aText);
        return sentences;
    }

    public static String[] posTag(String aSentence[], String aLang, String aResourceFolder) {
        String posTaggedVersion[] = null;
        if (itsPOSModel == null) {
            itsPOSModel = new POSModelLoader()
                    .load(new File(aResourceFolder + "/posModels/" + aLang + "-pos-maxent.bin"));
            itsPOSTagger = new POSTaggerME(itsPOSModel);
        }
        //PerformanceMonitor perfMon = new PerformanceMonitor(System.err, "sent");

        posTaggedVersion = itsPOSTagger.tag(aSentence);
        return posTaggedVersion;
    }

    public static String[] posTag(String aSentence[]) {
        String posTaggedVersion[] = null;
        if (itsPOSModel == null) {
            itsPOSModel = new POSModelLoader()
                    .load(new File(itsResourceFolder + "/posModels/" + itsLang + "-pos-maxent.bin"));
            itsPOSTagger = new POSTaggerME(itsPOSModel);
        }

        posTaggedVersion = itsPOSTagger.tag(aSentence);

        return posTaggedVersion;
    }

    public static void main2(String args[]) throws InvalidFormatException, IOException {
        LanguageProcessor posTagger = new LanguageProcessor();

        String text = "This time, it’s your turn: advise Parliament in the first LinkedIn discussion on an ongoing report. The rapporteur wants to hear your views @...(read more). --- Keywords ---";
        String lang = "en";
        String sentences[] = posTagger.sentenceDetect(text, lang, "D:\\work\\workspace\\POSTaggersALanguage");
        for (int i = 0; i < sentences.length; i++) {
            String string = sentences[i];
            System.out.println(string);
            String tokens[] = posTagger.tokenize(string, lang, "D:\\work\\workspace\\POSTaggersALanguage");
            for (int j = 0; j < tokens.length; j++) {
                String token = tokens[j];
                System.out.println(token);
            }
            String posTags[] = posTagger.posTag(tokens, lang, "D:\\\\work\\\\workspace\\\\POSTaggersALanguage");
            for (int j = 0; j < posTags.length; j++) {
                String string1 = posTags[j];
                System.out.println(string1);
            }
        }
    }

    public static void main4(String args[]) throws IOException {
        LanguageProcessor.setUp(Langs.EN, "nlpModels/");
        String nes[] = LanguageProcessor.getNE("I am going to London this afternoon after I will visit Michael Jackson".split("\\s"));
        for (int i = 0; i < nes.length; i++) {
            String string = nes[i];
            System.out.println(string);
        }
    }

    public static void main(String args[]) throws IOException {
        LanguageProcessor.setUp(Langs.EN, "nlpModels/");
        File files[] = new File("/home/ahmet/monicaSummaries/TxtSummary/").listFiles();
        for (int i = 0; i < files.length; i++) {
            File file = files[i];
            Vector<String> lines = Util.getFileContentAsVector(file.getAbsolutePath());
            StringBuffer text = new StringBuffer();
            for (int j = 0; j < lines.size(); j++) {
                String line = lines.get(j);
                if (line.trim().equals("")) {
                    continue;
                }
                text.append(line).append("\n");
            }
            String sentences[] = LanguageProcessor.sentenceDetect(text.toString());
            StringBuffer textNew = new StringBuffer();
            for (int j = 0; j < sentences.length; j++) {
                String sentence = sentences[j];
                textNew.append(sentence).append("\n");
            }
            Util.doSave("/home/ahmet/monicaSummaries/TxtSummary-LineSplit/" + file.getName(), textNew.toString());
        }
    }

}
