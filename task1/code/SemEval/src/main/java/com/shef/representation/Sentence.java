/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shef.representation;

import java.io.IOException;

/**
 *
 * @author ahmetaker
 */
public class Sentence {

    private String itsSentenceText = null;

    private String itsId = null;

    private String itsNEs[] = null;

    private String itsTokens[] = null;

    private String itsPOS[] = null;

    public String[] getPOS() {
        return itsPOS;
    }
    
    public void setPOS(String aPOSList[]) {
        itsPOS = aPOSList;
    }
    
    public String[] getTokens() {
        return itsTokens;
    }

    private StringBuffer buffer = null;

    public String getTokensSeperatedBySemicolon() throws IOException {
        if (buffer != null) {
            return buffer.toString();
        } else {
            buffer = new StringBuffer();
        }
        for (int i = 0; i < itsTokens.length; i++) {
            String token = itsTokens[i].toLowerCase();
            buffer.append(token).append(";");
        }
        return buffer.toString();
    }

    public void setTokens(String[] aTokens) {
        this.itsTokens = aTokens;
    }

    public Sentence() {
    }

    public Sentence(String aSentence, String anId) {
        itsSentenceText = aSentence;
        itsId = anId;
    }

    public Sentence(String aSentence, String anId, String aNEs[]) {
        this(aSentence, anId);
        itsNEs = aNEs;
    }

        public Sentence(String aSentence, String anId, String aNEs[], String aPOS[]) {
        this(aSentence, anId, aNEs);
        itsPOS = aPOS;
    }

                public Sentence(String aSentence, String anId, String aNEs[], String aPOS[], String aTokens[]) {
        this(aSentence, anId, aNEs, aPOS);
        itsTokens = aTokens;
    }

    public String[] getNEs() {
        return itsNEs;
    }

    public void setNEs(String aNEs[]) {
        this.itsNEs = aNEs;
    }

    public void setSentenceText(String aText) {
        itsSentenceText = aText;
    }

    public String getSentenceText() {
        return itsSentenceText;
    }

    public void setId(String anId) {
        itsId = anId;
    }

    public String getId() {
        return itsId;
    }
}
